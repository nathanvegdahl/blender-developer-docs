# Requirements for contributed Scripts

To prepare your script for submission into the Blender Add-ons Catalog
(to be distributed along with Blender) your script must

- contain [ Add-on Meta
  Information](../addon_meta_info.md) in a `bl_info` dictionary.
- define register() and unregister() functions.
- be documented on an associated [Wiki
  Page](https://en.blender.org/index.php/Dev:Py/Scripts/Guidelines/Addons/wikipage)
  in the Blender Wiki
- be evaluated and approved by another Blender Developer
- pass the Flake8 / other PEP 8 checker Tool For Style Guide Enforcement
- be compatible with the latest Blender release (for help updating
  scripts to run on 2.8x, consult this [page (Work in
  Progress)](../../../release_notes/2.80/python_api/index.md))
- Inclusion of binary data-files is to be avoided (`.blend` files for
  example).

## Style Recommendations

  
**The flake8 Tool**

The flake8 tool is currently used as a style checker for Python code.
However, other PEP 8 tools can be used instead, taking into account some
scripting specifics / style exemptions used for convenience. flake8
incorporates several scripts into one - providing additional
functionality like checking for unused variables. To install flake8,
follow the *Quickstart* instructions from the *Flake8 Checker* link at
the bottom of the page, so it is available system wide. The tool can be
run from the command line or setup to run from a specific IDE. To get
flake8 running in a specific editor, please consult that editor's
documentation.

**Here is the list of Style recommendations for Blender Add-ons**:

- Line length must not exceed 120 characters
- Use 4 spaces for indentation. No mixing of tabs and spaces allowed.
- Two lines gap between functions and classes, one maximum between
  blocks inside a function/class
- Spaces around operators
- No trailing spaces (at the end of a line or in empty lines of code)
- If possible, avoid single scripts longer than 1000 lines. Consider
  breaking them in sub-modules in a folder structure imported from an
  **__init__.py** instead.
- All *significant functions*<sup>1</sup>, classes, and functions within
  classes should have a docstring explaining
  - what they do,
  - which parameters they receive,
  - which parameters they return.
- Comments and variable names in US English.
- *Constant*<sup>2</sup> variable names all upper case.
- No reliance on deprecated parts of the Blender/Python API.

  
<sup>1</sup>: *Significant Functions*: For our purposes, a significant
function requiring a docstring is a function that's purpose and
functionality will not be obvious to a python programmer unfamiliar with
the code at first glance. Even fairly simple functions should have a
docstring if they are more then a few lines

<sup>2</sup>: *Constants*: Python has no constants, this refers to
variables that do not change in the script. For some example names: PI,
EUL for a small number, RAD_TO_DEG, IDENTITY_MATRIX4X4.

## Criteria to be checked before evaluation

Below you find a list of evaluation properties which should apply to
your Script. Please make sure that your Script matches the points nicely
before you contact a reviewer. This will save a lot of time for
everybody and it will give you a good chance to get your Work published
quickly:

- Robustness
- Maintainability (readability)
- Correctness and completeness
- Functionality
- Documentation
- Consistency (the interface and functionality should try to match with
  the existing feel and conventions of Blender).

## How to get your Script published in Blender

If you are finally ready to publish your addon on the Blender Foundation
servers, get started by reading the formal [Process to Submit
Addons](../index.md) to Blender repositories.

## Special case: Using Custom Icons and images

If you want to use custom icons in your addon (available from Blender
2.75), you also need to observe these guidelines:

- Do not use these for advertising, logos, or other non-functional use
  cases.
- Use a minimal number of custom icons. This helps to keep Blender's
  interface uniform and uncluttered.
- Only use custom images in a way that is compatible with how the rest
  of Blender works - they should seem like part of the default interface
  we provide for everyone.
- Only use the feature within the context and UI of your own Add-on. You
  cannot rely on other add-ons to have this already preloaded. This
  feature also does not allow replacing of the default icons.
- Do not force preview image creation outside of the add-on dir, except
  when explicitly asked for by the user in the UI.
- The default icon size in Blender is 32x32 and for previews it's
  256x256. Your images will be scaled to fit these sizes on their
  maximum side.

# See also

## Addons usage

[Blender Manual page on addons](https://docs.blender.org/manual/en/dev/editors/preferences/addons.html)  
General information about addon **usage**

## Coding style

[Style Guide for Python Code](http://www.python.org/dev/peps/pep-0008/)  
Recommended coding conventions for submitted Python code

<!-- -->

[Flake8 Checker](https://pypi.python.org/pypi/flake8)  
A tool to check Python code to see if it conforms to the PEP 8 style
conventions listed in the link just above this one

<!-- -->

[PEP8 online check](http://pep8online.com/)  
An Online PEP 8 Python code checker
