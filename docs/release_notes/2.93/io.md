# Blender 2.93: Pipeline, Assets & I/O

## glTF 2.0

### Importer

- Fix morph target import when named 'basis'
  (blender/blender-addons@7521a4e27be9)
- Fix shapekey import with negative weight
  (blender/blender-addons@0dc2141207df)
- Better extra error management
  (blender/blender-addons@30012da83522)

### Exporter

- New feature: export 'Loose Points' and 'Loose Edges'
  (blender/blender-addons@8d24537a6e9c)
- New option to export only visible/renderable/active collection
  (blender/blender-addons@91f57b489943)
- Draco : Increase maximal compression limit
  (blender/blender-addons@55ec46913b99)
- Inverse matrix only when there is a parent
  (blender/blender-addons@8adb0dce852d)
- Hook entire glTF data for extensions
  (blender/blender-addons@4e1ab4a386d5)
- Fix shapekey export in some cases
  (blender/blender-addons@5ef60cb44a0c)
- Fix exporting with draco when duplicate geometry
  (blender/blender-addons@3d7d5cead543)
- Fix crash when mesh is not created, without crash
  (blender/blender-addons@f172f7724749)
