# Blender 4.0: Cycles

## Light and Shadow Linking

With light linking, lights can be set to affect only specific objects in
the scene. Shadow linking additionally gives control over which objects
acts as shadow blockers for a light.
(blender/blender@ba3f26fac58eb3a37b2cfac38b30d1fd6d1a4094)

This adds more artistic control for lighting by breaking the laws of
physics. For example the environment and characters in a shot might have
different light setups. A character could have a dedicated linked rim
light to make it stand out, and shadow linking could be used to ensure
no objects from the environment block it.

See the [Light Linking in the user
manual](https://docs.blender.org/manual/en/4.0/render/cycles/object_settings/light_linking.html)
for more details.

|Light Linking for Rim Shading|No Rim Light|Rim Light on All Objects|
|-|-|-|
|![](../../images/Cycles4.0-monkey-link-on.png)|![](../../images/Cycles4.0-monkey-link-none.png)|![](../../images/Cycles4.0-monkey-link-all.png)|

In a future version we plan to support linking to the world lighting,
and add a more convenient user interface to view and manage all light
links in a scene.

### Examples

|All Lights|Light Linking|
|-|-|
|![](../../images/Cycles4.0-car-link-off.jpeg)|![](../../images/Cycles4.0-car-link-on.jpeg)|

Different light per car part, to finely control shape of reflections.
Scene by Fernando Alcala using asset by LRosario.

|All Lights|Light Linking|
|-|-|
|![](../../images/Cycles4.0-creature-link-off.jpeg)|![](../../images/Cycles4.0-creature-link-on.jpeg)|

Light linking used to artificially eliminate reflections on the eyes and tree branch.  
Scene by Alaska Young and Daniel Bystedt.

|Character|Background|Volume|Combined|
|-|-|-|-|
|![](../../images/Cycles4.0-char-link-char.jpeg){style="width:200px;"}|![](../../images/Cycles4.0-char-link-floor.jpeg){style="width:195px;"}|![](../../images/Cycles4.0-char-link-volume.jpeg){style="width:200px;"}|![](../../images/Cycles4.0-char-link-on.jpeg){style="width:200px;"}|

Distinct lights for character, background and volume.  
Scene by Fernando Alcala using Blendswap asset by adi.

## Path Guiding

Path Guiding now works on glossy surfaces in addition to diffuse
surfaces. This can significantly reduce noise on glossy surfaces, and
find otherwise missing glossy light paths. (blender/blender!107782)

|Diffuse Guiding|New Glossy Guiding|
|-|-|
|![](../../images/Cycles4.0-path-guiding-diffuse.jpg){style="width:400px;"}|![](../../images/Cycles4.0-path-guiding-mis.jpg){style="width:400px;"}|

Scene by Fernando Alcala and LRosario, made using assets from Blendswap.

## Multiple Scattering GGX

The Multiscattering GGX implementation was replaced by an approximation
based on ["Practical multiple scattering compensation for microfacet
models" by Emmanuel
Turquin](https://blog.selfshadow.com/publications/turquin/ms_comp_final.pdf).
(blender/blender@888bdc1419a2cd99284060a64d1df0c4397d3ac5)

- This removes the performance and noise disadvantage that used to be
  associated with Multiscattering GGX, making it a safe default pick.
- While the directional distribution is no longer theoretically ideal,
  which can lead to slight differences in rendered images, the overall
  effect of the multiscattering correction (preserving energy, avoiding
  darkening at high roughness, and increasing saturation at high
  roughness) still applies.
- For highly anisotropic materials, the compensation might not be
  perfect. This is a limitation of the currently known approximation
  methods.

## Principled Hair BSDF

A new variant called "Huang" is added, based on the paper [A
Microfacet-based Hair Scattering
Model](https://doi.org/10.1111/cgf.14588) by Weizhen Huang, Matthias B.
Hullin and Johannes Hanika (blender/blender!105600).
The previous Principled Hair BSDF is now the "Chiang" variant.

- Supports elliptical cross-sections, adding more realism as human hairs
  are usually elliptical. The orientation of the cross-section is
  aligned with the curve normal, which can be adjusted using geometry
  nodes. Default is minimal twist.
- This is a far-field model, as opposed to the near-field Principled
  Hair BSDF model. The hair is expected to be less noisy, but lower
  roughness values takes longer to render due to numerical integration
  along the hair width. The hair also appears to be flat when viewed
  up-close.
- Gives nicer focused reflection when viewed against the light.

For more detailed comparisons, please refer to the original paper and
the [Principled Hair user
manual](https://docs.blender.org/manual/en/4.0/render/shader_nodes/shader/hair_principled.html#huang-model).

|Chiang Model|Microfacet Based Huang Model|
|-|-|
|![](../../images/Cycles4.0-hair-chiang.jpg)|![](../../images/Cycles4.0-hair-huang.jpg)|
|![](../../images/Cycles4.0-hair2-chiang.jpg)|![](../../images/Cycles4.0-hair2-huang.jpg)|

Scene by Simon Thommes and Fernando Alcala.

## Open Shading Language

Most Cycles-specific microfacet closures were removed since they are
redundant now that Cycles implements the generic `microfacet()`
closure of the OSL standard.
(blender/blender@888bdc1419a2cd99284060a64d1df0c4397d3ac5)

- Specifically, the `microfacet()` closure supports the distributions
  `beckmann`, `sharp`, `ashikhmin_shirley`, `ggx`, `multi_ggx`
  and `clearcoat`. The `refract` argument supports `0` for pure
  reflection, `1` for pure refraction, and `2` for both (based on
  dielectric Fresnel according to the specified IOR).
- Additionally, the three MaterialX microfacet closures
  `dielectric_bsdf()`, `conductor_bsdf()` and
  `generalized_schlick_bsdf()` are provided. These only support
  `beckmann`, `ggx` and `multi_ggx` distributions.
- Two Cycles-specific closures remain, but they are for internal use and
  should not be relied upon by user-created OSL shaders.

## Metal Hardware Raytracing

The new Apple M3 processor supports hardware ray-tracing. Cycles takes
advantage of this by default, with the MetalRT in the GPU device
preferences set to "Auto".

For the M1 and M2 processors MetalRT is no longer an experimental
feature, and fully supported now. However it is off by default, as
Cycles' own intersection code still has better performance when there is
no hardware ray-tracing.

## Other

- Performance improvements to geometry upload from [Blender 3.6](../3.6/cycles.md#performance)
  continued, with a 1.76x improvement measure for a large mesh
  (blender/blender@2fac2228d05e165aeb8793cbdf5f1910710ff574).
- Metal AMD GPU rendering has a significant performance regression in
  scenes that use the Principled Hair BSDF, in addition to existing lack
  of support for the light tree and shadow caustics. Due to GPU driver
  limitations that are unlikely to be fixed, it is expected that AMD GPU
  rendering on macOS will be disabled entirely in a future release.
- Support for rendering with AMD RDNA2 and RDNA3 APUs is added.
