# Blender 2.83: More Features

## Undo

Performance of undo in Object and Pose mode was significantly improved,
particularly for scenes with many objects. Further optimizations are
planned for future releases.

This optimization is enabled by default. If there are stability issues,
it is possible to use the slower undo system by enabling Developer
Extras in the preferences and changing the option in the Experimental
section. See [the design task](https://developer.blender.org/T60695) for
more details.

## 3D Viewport

- Improve selection with many small objects in the 3D viewport.
  (blender/blender@3685347b4172)
- Revamped color management, with compositing now performed in linear
  space for overlays and grease pencil for more accurate results.

## Video Sequencer

- Disk Cache to store cached frames on disk instead of only in memory.
  It can be enabled in Preferences \> System \> Video Sequencer.
  (blender/blender@348d2fa09e0c)
- Revamped sequence strip drawing
  (blender/blender@271231f58ee3)
- Show f-curves for opacity and volume values on the strips
  (blender/blender@d0d20de183f1)
- Adjust Last Operation panel
  (blender/blender@a4cf2cf2decc).
- Toolbar and color sample tool
  (blender/blender@6a49161c8c60)
  (blender/blender@68ba6378b5b1)
- Option to select handles with box selection
  (blender/blender@5314161491d4)

## Import & Export

### Universal Scene Description

- Metaballs can now also be exported to USD
  (blender/blender@4b2b5fe4b8d3b).

### Alembic

- Improved import and export of camera transformations. Children of
  cameras are now handled properly
  (blender/blender@7c5a44c71f).
- Animated UV coordinates are now exported and imported correctly.
  (blender/blender@65574463fa9)
  (blender/blender@94cbfb71b)

### glTF

- Exporter
  - Manage collection / collection instance / proxy
    (blender/blender-addons@191bcee579b3)
  - Improve texture export performance
    (blender/blender-addons@dfadb306b0e9)
  - KHR_materials_clearcoat export
    (blender/blender-addons@65bad4212eb9)
  - Better image option
    (blender/blender-addons@872e3e6afac9)
  - Don't take mute strip into account
    (blender/blender-addons@22424950a3f6)
  - Fix bone cache issue
    (blender/blender-addons@cc3e0cce4a53,
    blender/blender-addons@ffd8a687247d)
  - Fix normals for skinned meshes
    (blender/blender-addons@ec5ca6a6cb54)
  - Manage user extension at glTF level
    (blender/blender-addons@1ae12e6f5d3c)
  - Animation general improvment
    (blender/blender-addons@e5bde5ec2e68,
    blender/blender-addons@f3683cf7bf74,
    blender/blender-addons@2425a37ae57d,
    blender/blender-addons@4ef5cd10f689,
    blender/blender-addons@68d5831729bf,
    blender/blender-addons@b72a6c057999)
  - Ability to undo an export
    (blender/blender-addons@d52854d420bd)
  - Export extra for bones
    (blender/blender-addons@15d56ea627d9)
  - Don't combine image names if they're all from the same file
    (blender/blender-addons@0a361e787dbd)
  - Normalized normals for skinned mesh
    (blender/blender-addons@b42aecdca411)
  - Fix Vertex Group export in some cases
    (blender/blender-addons@dc4c83cffc6b)
  - fix bug when apply modifier + disable skinning
    (blender/blender-addons@69d4daee6ffe)
  - Less naive file format detection for textures
    (blender/blender-addons@7a3fdf08f3fe)
  - Fix export some animation interpolations
    (blender/blender-addons@18a0f95a8482)
- Importer
  - Fix skinning & bone hierarchy
    (blender/blender-addons@75855d723895,
    blender/blender-addons@09bbef319f5e,
    blender/blender-addons@39e2149b6c62,
    blender/blender-addons@e4269fc795cf)
  - Better bone rotation management & bind pose
    (blender/blender-addons@6e7dfdd8a91f,
    blender/blender-addons@96a4166c8de2,
    blender/blender-addons@38531bbfa567)
  - Yfov near / far camera import
    (blender/blender-addons@d8aa24a9321e,
    blender/blender-addons@43148f17496c)
  - Rewrite material import
    (blender/blender-addons@2caaf64ab10b)
  - Manage morph weights at node level
    (blender/blender-addons@ee61a3a692d3)
  - Simplify/cleanup image import
    (blender/blender-addons@e88c7ee2a7c0)
  - Omit texture mapping & UVMap node when not needed
    (blender/blender-addons@b629ab427c4e)
  - Use emission/alpha sockets on Principled node
    (blender/blender-addons@b966913caab3)
  - Manage texture wrap modes
    (blender/blender-addons@d0dee32d2b5f)
  - Better retrieve camera & lights names
    (blender/blender-addons@fd6bfbb44af4)
  - Performance improvement importing meshes & normals
    (blender/blender-addons@f5f442a7a665)
  - Rename option from export_selected to use_selection
    (blender/blender-addons@120d313b842e,
    blender/blender-addons@a35d66c1337a)
  - Bone & Animation code cleanup & checks
    (blender/blender-addons@b4fc9fbcf4e2,
    blender/blender-addons@9920e00e5fbe,
    blender/blender-addons@0e2a855c40c6,
    blender/blender-addons@8db785ea740b,
    blender/blender-addons@f4e12a20f5d0)
  - Add extension to tmp file generated when importing textures
    (blender/blender-addons@ea2965438756)
  - Improve node names
    (blender/blender-addons@02ca41d48ea3)
  - Use friendly filenames for temp image files
    (blender/blender-addons@45afacf7a630)
  - Better material names when using color vertex
    (blender/blender-addons@0650f599aa17)
  - Fix bug on windows, use absolute paty
    (blender/blender-addons@520b6af2be38)

## Miscellaneous

- View Layers: when adding a new view layer, it is now possible to copy
  settings from currently active one, or to create an 'empty' one (where
  all collections are disabled)
  (blender/blender@d1972e50cbef).
- Windows: support for high resolution tablet pen events when using
  Windows Ink, so strokes more accurately match the pen motion. This was
  previously only supported on Linux.
  (blender/blender@d571d61)
- Freestyle: option to render freestyle to a separate pass.
